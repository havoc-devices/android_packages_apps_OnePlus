#
# Copyright (C) 2021 Havoc-OS
#
# Licensed under the Apache License, Version 2.0 (the License);
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an AS IS BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#

LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
LOCAL_MODULE := OnePlusCamera
LOCAL_MODULE_TAGS := optional
LOCAL_SRC_FILES := OnePlusCamera.apk
LOCAL_CERTIFICATE := PRESIGNED
LOCAL_MODULE_CLASS := APPS
LOCAL_SYSTEM_EXT_MODULE := true
LOCAL_OVERRIDES_PACKAGES := Camera2 Snap
LOCAL_PRIVILEGED_MODULE := true
LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)
LOCAL_REQUIRED_MODULES := hiddenapi-package-whitelist-oem libsnpe_dsp_v66_domains_v2_skel privapp-permissions-oem-system_ext
include $(BUILD_PREBUILT)

LIBSNPE_SYMLINK := $(TARGET_OUT_SYSTEM_EXT)/priv-app/OnePlusCamera/lib/arm64/libsnpe_dsp_v66_domains_v2_skel.so
$(LIBSNPE_SYMLINK): $(LOCAL_INSTALLED_MODULE)
	@echo "libsnpe_dsp_v66_domains_v2_skel link: $@"
	@mkdir -p $(dir $@)
	@rm -rf $@
	$(hide) ln -sf /system_ext/lib64/libsnpe_dsp_v66_domains_v2_skel.so $@

ALL_DEFAULT_INSTALLED_MODULES += $(LIBSNPE_SYMLINK)
